/*
 * peas-perl-internal.c
 * This file is part of libpeas
 *
 * Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                    <neva_blyad@lovecri.es>
 *
 * libpeas is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libpeas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "peas-perl-internal.h"

#include <gio/gio.h>

EXTERN_C void xs_init (pTHX);

static SV *internal_hooks = NULL;

gboolean
peas_perl_internal_setup (void)
{
  const gchar *prgname;
  gchar *args[] = { "", "-MGlib::Object::Introspection", "-e", "0", NULL };
  GBytes *internal_perl;
  gconstpointer code;

#define goto_error_if_failed(cond) \
  G_STMT_START { \
    if (G_UNLIKELY (!(cond))) \
      { \
        g_warn_if_fail (cond); \
        goto error; \
      } \
  } G_STMT_END

  /**
   * Run Perl initial configuration code
   */
  if (perl_parse (my_perl,
                  xs_init,
                  G_N_ELEMENTS (args) - 1,
                  args,
                  NULL) != 0)
    {
      g_critical ("Can't execute Perl bootstrap code");
      goto error;
    }

  /**
   * Load Perl helper subroutines
   */
  internal_perl = g_resources_lookup_data ("/org/gnome/libpeas/loaders/"
                                            "perl/"
                                            "internal.pm",
                                            G_RESOURCE_LOOKUP_FLAGS_NONE,
                                            NULL);
  goto_error_if_failed (internal_perl != NULL);
  code = g_bytes_get_data (internal_perl, NULL);
  eval_pv (code, TRUE);

  /**
   * ... and create reference to them
   */
  internal_perl = g_resources_lookup_data ("/org/gnome/libpeas/loaders/"
                                            "perl/"
                                            "internal.pl",
                                            G_RESOURCE_LOOKUP_FLAGS_NONE,
                                            NULL);
  goto_error_if_failed (internal_perl != NULL);
  code = g_bytes_get_data (internal_perl, NULL);
  eval_pv (code, TRUE);

  /**
   * Some constants are needed,
   * Perl plugin should have ability to use them
   */
  SV *command = newSV (0);

  prgname = g_get_prgname ();
  prgname = prgname == NULL ? "" : prgname;

  sv_setpvf (command, "use constant PRGNAME => '%s';", prgname);
  eval_sv (command, TRUE);
  sv_setpvf (command, "use constant GETTEXT_PACKAGE => '%s';", GETTEXT_PACKAGE);
  eval_sv (command, TRUE);
#ifndef G_OS_WIN32
  sv_setpvf (command, "use constant PEAS_LOCALEDIR => '%s';", PEAS_LOCALEDIR);
  eval_sv (command, TRUE);
#endif

  SvREFCNT_dec (command);

  /**
   * We should remember also the scalar with reference to the object of Hooks
   * class. We'll use it in future for accessing the Hooks subroutines.
   */
  internal_hooks = get_sv ("hooks", 0);
  goto_error_if_failed (internal_hooks != NULL);

#undef goto_error_if_failed

  return TRUE;

error:

  internal_hooks = NULL;

  return FALSE;
}

void
peas_perl_internal_shutdown (void)
{
  internal_hooks = NULL;
}

SV *
peas_perl_internal_call (const gchar *name,
                         gboolean should_return_value, // If TRUE, do not forget to free the returned SV with sv_free()
                         const gchar *format,
                         ...)
{
  dSP;

  va_list args;
  gchar arg_type;

  union
    {
      const gchar *str;
      UV uv;
    } arg;

  SV *res;

  ENTER;
  SAVETMPS;
  PUSHMARK (SP);
  XPUSHs (internal_hooks);

  va_start (args, format);
  arg_type = *format++;

  while (arg_type != '\0')
    {
      switch (arg_type)
        {
        case 's':
          arg.str = va_arg (args, gchar *);
          mXPUSHs (newSVpv (arg.str, 0));
          break;
        case 'd':
          arg.uv = va_arg (args, UV);
          mXPUSHs (newSVuv (arg.uv));
          break;
        case 'c':
        default:
          arg.str = va_arg (args, gchar *);
          g_critical ("Parameter '%s' in Perl internal call with argument format '%s' is not supported yet", arg.str, format);
          PUTBACK;
          res = NULL;
          goto exit;
        }

      arg_type = *format++;
    }

  va_end (args);

  PUTBACK;

  if (should_return_value)
    {
      call_method (name, G_SCALAR);
      SPAGAIN;
      res = POPs;
      res = newSVsv (res);
      PUTBACK;
    }
  else
    {
      call_method (name, G_DISCARD | G_VOID);
      res = NULL;
    }

exit:

  FREETMPS;
  LEAVE;

  return res;
}
