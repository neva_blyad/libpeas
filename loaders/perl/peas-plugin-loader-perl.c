/*
 * peas-plugin-loader-perl.c
 * This file is part of libpeas
 *
 * Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                    <neva_blyad@lovecri.es>
 *
 * libpeas is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libpeas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "peas-plugin-loader-perl.h"
#include "peas-perl-internal.h"
#include "libpeas/peas-plugin-info-priv.h"

#include <EXTERN.h> /* from the Perl distribution */
#include <perl.h>   /* from the Perl distribution */

typedef struct {
  guint n_loaded_plugins;

  guint init_failed : 1;
  guint must_finalize_perl : 1;
} PeasPluginLoaderPerlPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (PeasPluginLoaderPerl,
                            peas_plugin_loader_perl,
                            PEAS_TYPE_PLUGIN_LOADER)

#define GET_PRIV(o) \
  (peas_plugin_loader_perl_get_instance_private (o))

PerlInterpreter *my_perl; /* The Perl interpreter */

static GQuark quark_extension_type = 0;

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
  peas_object_module_register_extension_type (module,
                                              PEAS_TYPE_PLUGIN_LOADER,
                                              PEAS_TYPE_PLUGIN_LOADER_PERL);
}

static GType
find_perl_extension_type (GType exten_type, PeasPluginInfo *info)
{
  const gchar *module_name;
  const gchar *type_name;

  struct
    {
      SV    *ptr;
      gchar *str;
    } type_name_out;

  GType type_out;

  module_name = peas_plugin_info_get_module_name (info);
  type_name = g_type_name (exten_type);

  type_name_out.ptr = peas_perl_internal_call ("find_extension_type", TRUE, "ss",
                                               module_name, type_name);

  if (type_name_out.ptr == NULL)
    {
      type_out = G_TYPE_INVALID;
    }
  else
    {
      type_name_out.str = SvPV_nolen (type_name_out.ptr);
      type_out = g_type_from_name (type_name_out.str);
      sv_free (type_name_out.ptr);
    }

  return type_out;
}

static gboolean
peas_plugin_loader_perl_provides_extension (PeasPluginLoader *loader,
                                            PeasPluginInfo   *info,
                                            GType             exten_type)
{
  GType type_out;

  type_out = find_perl_extension_type (exten_type, info);
  return type_out != G_TYPE_INVALID;
}

G_GNUC_BEGIN_IGNORE_DEPRECATIONS
static GObject *
peas_plugin_loader_perl_create_extension (PeasPluginLoader *loader,
                                          PeasPluginInfo   *info,
                                          GType             exten_type,
                                          guint             n_parameters,
                                          GParameter       *parameters)
{
  GObject *object;
  const gchar *module_name;
  GType the_type;

  object = NULL;
  module_name = peas_plugin_info_get_module_name (info);

  the_type = find_perl_extension_type (exten_type, info);
  if (the_type == G_TYPE_INVALID)
    goto out;

  object = g_object_newv (the_type, n_parameters, parameters);
  if (object == NULL)
    goto out;

  /* Sink floating references if necessary */
  if (g_object_is_floating (object))
    g_object_ref_sink (object);

  /* We have to remember which interface we are instantiating
   * for the deprecated peas_extension_get_extension_type().
   */
  g_object_set_qdata (object, quark_extension_type,
                      GSIZE_TO_POINTER (exten_type));

  /* Now extension exists.
   * Let Perl to know about this.
   *
   * Also set the plugin info as an attribute of the instance.
   *
   * TODO:
   * pass (already Perl-wrappered?) GBoxed structure here
   */
  peas_perl_internal_call ("create_extension_object_from_existing", FALSE, "sdd",
                           module_name, object, info);

out:

  return object;
}
G_GNUC_END_IGNORE_DEPRECATIONS

static gboolean
peas_plugin_loader_perl_load (PeasPluginLoader *loader,
                              PeasPluginInfo   *info)
{
  PeasPluginLoaderPerl *pmloader = PEAS_PLUGIN_LOADER_PERL (loader);
  PeasPluginLoaderPerlPrivate *priv = GET_PRIV (pmloader);
  const gchar *module_dir, *module_name;
  SV *res;

  module_dir = peas_plugin_info_get_module_dir (info);
  module_name = peas_plugin_info_get_module_name (info);

  res = peas_perl_internal_call ("load", TRUE, "sss",
                                 info->filename,
                                 module_dir, module_name);

  if (res != NULL && SvIV (res) == 1)
    {
      priv->n_loaded_plugins += 1;
      sv_free (res);

      return TRUE;
    }

  return FALSE;
}

static void
peas_plugin_loader_perl_unload (PeasPluginLoader *loader,
                                PeasPluginInfo   *info)
{
  PeasPluginLoaderPerl *pmloader = PEAS_PLUGIN_LOADER_PERL (loader);
  PeasPluginLoaderPerlPrivate *priv = GET_PRIV (pmloader);
  const gchar *module_dir, *module_name;

  module_dir = peas_plugin_info_get_module_dir (info);
  module_name = peas_plugin_info_get_module_name (info);

  peas_perl_internal_call ("unload", FALSE, "sss",
                           info->filename,
                           module_dir, module_name);

  /* We have to use this as a hook as the
   * loader will not be finalized by applications
   */
  if (--priv->n_loaded_plugins == 0)
    peas_perl_internal_call ("all_plugins_unloaded", FALSE, "");
}

static gboolean
peas_plugin_loader_perl_initialize (PeasPluginLoader *loader)
{
  PeasPluginLoaderPerl *pmloader = PEAS_PLUGIN_LOADER_PERL (loader);
  PeasPluginLoaderPerlPrivate *priv = GET_PRIV (pmloader);

  gint  argc;
  gchar *argv[1];
  gchar **argv_ptr;
  gchar **env;

  if (priv->must_finalize_perl)
  {
    g_warning ("The Perl interpreter has been executed already");

    return TRUE;
  }

  priv->must_finalize_perl = TRUE;

  /* Perl initialization */
  argc     = 0;
  argv[0]  = NULL;
  argv_ptr = argv;
  env      = g_get_environ ();

  PERL_SYS_INIT3 (&argc, &argv_ptr, &env);
  my_perl = perl_alloc ();
  if (my_perl == NULL)
    {
      g_critical ("Can't allocate the Perl interpreter, no memory");
      goto perl_alloc_error;
    }

  perl_construct (my_perl);

  /* Must be done last, finalize() depends on init_failed */
  if (!peas_perl_internal_setup ())
    {
      /* Already warned */
      goto perl_init_error;
    }

  PL_exit_flags |= PERL_EXIT_DESTRUCT_END;

  if (perl_run (my_perl) != 0)
    {
      g_critical ("Can't run the Perl interpreter");
      goto perl_init_error;
    }

  return TRUE;

perl_init_error:

  priv->init_failed = TRUE;
  g_warning ("Please check the installation of all the Perl "
             "related modules required by libpeas and try again");

perl_alloc_error:

  return FALSE;
}

static void
peas_plugin_loader_perl_init (PeasPluginLoaderPerl *loader)
{
}

static void
peas_plugin_loader_perl_finalize (GObject *object)
{
  PeasPluginLoaderPerl *pmloader = PEAS_PLUGIN_LOADER_PERL (object);
  PeasPluginLoaderPerlPrivate *priv = GET_PRIV (pmloader);

  g_warn_if_fail (priv->n_loaded_plugins == 0);

  if (priv->must_finalize_perl)
    {
      if (!priv->init_failed)
        {
          peas_perl_internal_call ("exit", FALSE, "");
          peas_perl_internal_shutdown ();
          perl_destruct (my_perl);
          perl_free (my_perl);
        }

      PERL_SYS_TERM ();
    }

  G_OBJECT_CLASS (peas_plugin_loader_perl_parent_class)->finalize (object);
}

static void
peas_plugin_loader_perl_class_init (PeasPluginLoaderPerlClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  PeasPluginLoaderClass *loader_class = PEAS_PLUGIN_LOADER_CLASS (klass);

  quark_extension_type = g_quark_from_static_string ("peas-extension-type");

  object_class->finalize = peas_plugin_loader_perl_finalize;

  loader_class->initialize = peas_plugin_loader_perl_initialize;
  loader_class->load = peas_plugin_loader_perl_load;
  loader_class->unload = peas_plugin_loader_perl_unload;
  loader_class->create_extension = peas_plugin_loader_perl_create_extension;
  loader_class->provides_extension = peas_plugin_loader_perl_provides_extension;
  loader_class->garbage_collect = NULL; /* No garbage collector */
}
