=pod

=encoding UTF-8

=head1 NAME

libpeas
peas-perl-internal.pl

=head1 DESCRIPTION

Perl plugin support for libpeas

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

libpeas is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

libpeas is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Try::Tiny;

# Internal hooks
try
{
    require 'peas_perl_internal.pm';
    peas_perl_internal->import;
}
catch
{
    # Assume that peas_perl_internal module already had been loaded in RAM
    # by C bootstrap code
};

# Declare package
package main;

# We'll use this global scalar from C to access various helper Perl methods
our $hooks = peas_perl_internal->new;

# vim: ts=4:sw=4:sts=4:et
