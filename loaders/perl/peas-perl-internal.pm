=pod

=encoding UTF-8

=head1 NAME

libpeas
peas-perl-internal.pm

=head1 DESCRIPTION

Perl plugin support for libpeas

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                   <neva_blyad@lovecri.es>

libpeas is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

libpeas is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We are serious about Perl
use strict;
use warnings;

# Handle UTF-8 literals
use utf8;

# CPAN modules
use Glib;
use Glib::Object::Introspection;
use Glib::Object::Subclass;
use Gtk3;

# Setup GIR instrospection
Glib::Object::Introspection->setup(
    basename => 'Peas',
    version  => '1.0',
    package  => 'libpeas-1.0'
);

Glib::Object::Introspection->setup(
    basename => 'PeasGtk',
    version  => '1.0',
    package  => 'libpeas-gtk-1.0'
);

# Declare package
package peas_perl_internal v0.1.0;

# CPAN modules
#use Data::Dumper;
use Try::Tiny;

# Hooks object constructor
sub new
{
    my $self = {};
       $self = bless $self;

    return $self;
}

# Load plugin into memory
sub load
{
    my $self        = shift;
    my $filename    = shift;
    my $module_dir  = shift;
    my $module_name = shift;

    # Add directory where to find the plugin
    #push @INC unless $module_dir ~~ @INC; # Still experimental operator, don't use

    my $found;

    foreach my $inc (@INC)
    {
        if ($inc eq $module_dir)
        {
            $found = 1;
            last;
        }
    }

    push @INC, $module_dir unless $found;

    # Load the plugin
    try
    {
        require "$module_name.pm";
    }
    catch
    {
        die "Error loading plugin $module_name: $_\n";
    };

    # Import symbols of its packages.
    #
    # Ensure all the packages (i. e. classes) are registered with GObject type
    # system.
    no strict 'refs';
    my $fq = "${module_name}::";
    my $pkgs = \%{$fq};
    use strict 'refs';

    foreach my $pkg (sort keys %$pkgs)
    {
        # Package name
        my $pkg_fq = "$fq$pkg";
           $pkg_fq = substr $pkg_fq, 0, -2;

        # Import symbols
        #$pkg->import; # Do not pollute main namespace

        # Register type
        try
        {
            Glib::Type->list_ancestors($pkg_fq);
        }
        catch
        {
            die "Dynamic typing error in plugin $module_name: $_\n";
        };
    }

    return 1;
}

# Unload plugin from memory
sub unload
{
    my $self        = shift;
    my $filename    = shift;
    my $module_dir  = shift;
    my $module_name = shift;

    # Unload the plugin
    try
    {
        exists $INC{"$module_name.pm"} or die 'not loaded';
        delete $INC{"$module_name.pm"};

        no strict 'refs';
        my $obj = \%{"${module_name}::"}{'obj'};
        $$obj->DESTROY;
        use strict 'refs';
    }
    catch
    {
        die "Error loading plugin $module_name: $_\n";
    };

    # Unimport symbols of its packages
    no strict 'refs';

    try
    {
        # Unimport symbols
        my $fq = "${module_name}::";
        my $pkgs = \%{$fq};

        foreach my $pkg ($fq, sort keys %$pkgs)
        {
            # Package name
            my $pkg_fq = "$fq$pkg";
               $pkg_fq = substr $pkg, 0, -2;

            # Unimport symbols
            #$pkg_fq->unimport; # Hadn't imported before
        }

        # Undefine and undeclare all symbols
        $_ = $fq;

        undef ${$_};
        undef &{$_};
        undef @{$_};
        undef %{$_};
        undef *{$_};

        delete $::{$_};
    }
    catch
    {
        die "Error cleaning plugin $module_name: $_\n";
    };

    use strict 'refs';

    # Remove directory where the plugin was searched
    my $idx = 0;
    $idx++ until $INC[$idx] eq $module_dir;
    splice @INC, $idx, 1;
}

# Find out if the loaded plugin supports specified GObject type or not
# (i. e. derived from it).
#
# Returns the plugin final type as it named in GObject type system.
# (It differs from the plugin package name a little bit,
# e. g. Glib::Type::register_object() substitute Perl :: package separators by
# __.)
sub find_extension_type
{
    my $self        = shift;
    my $module_name = shift;
    my $type        = shift;

    # GIR lookup table
    my $gir_hash = \%Glib::Object::Introspection::_BASENAME_TO_PACKAGE;
    my $gir_arr  = [ reverse sort keys %$gir_hash ];

    # Parent package name
    my $parent = $type;

    foreach my $key (@$gir_arr)
    {
        my $val = $gir_hash->{$key};

        my $replaced = $parent =~ s|^\Q$key\E|${val}::|;
        last if $replaced;
    }

    # Package name
    my $pkg = $type;

    foreach my $key (@$gir_arr)
    {
        my $replaced = $pkg =~ s|^\Q$key\E|${module_name}::|;
        last if $replaced;
    }

    # Package parents
    my $found;

    no strict 'refs';
    my $parents = \@{"${pkg}::ISA"};
    use strict 'refs';

    foreach my $parent_ (@$parents)
    {
        if ($parent_ eq $parent)
        {
            $found = 1;
            last;
        }
    }

    # GObject type
    if ($found)
    {
        $type = $pkg;
        $type =~ s|::|__|g;
    }
    else
    {
        $type = undef;
    }

    return $type;
}

# Assume plugin is already allocated and initialized.
# Let Perl to know about this.
sub create_extension_object_from_existing
{
    my $self        = shift;
    my $module_name = shift;
    my $c_ptr       = shift;
    my $info_ptr    = shift;

    my $obj;
    my $info;

    try
    {
        no strict 'refs';
        $obj = \%{"${module_name}::"}{'obj'};
        use strict 'refs';

        $$obj = Glib::Object->new_from_pointer($c_ptr, Glib::FALSE);
    }
    catch
    {
        die "Error in plugin $module_name constructor: $_\n";
    };

    # TODO:
    # save Perl-wrappered GBoxed structure here
    ${$obj}->{plugin_info} = $info;
}

# All plugins have been unloaded
sub all_plugins_unloaded
{
    my $self = shift;
}

# Shutdown
sub exit
{
    my $self = shift;
}

1;

# vim: ts=4:sw=4:sts=4:et
