/*
 * peas-perl-internal.h
 * This file is part of libpeas
 *
 * Copyright (C) 2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
 *                                    <neva_blyad@lovecri.es>
 *
 * libpeas is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * libpeas is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA.
 */

#ifndef __PEAS_PYTHON_INTERNAL_H__
#define __PEAS_PYTHON_INTERNAL_H__

#include <glib.h>

#include <EXTERN.h> /* from the Perl distribution */
#include <perl.h>   /* from the Perl distribution */

G_BEGIN_DECLS

EXTERN_C PerlInterpreter *my_perl; /* The Perl interpreter */

gboolean  peas_perl_internal_setup    (void);
void      peas_perl_internal_shutdown (void);

SV       *peas_perl_internal_call     (const gchar *name,
                                       gboolean should_return_value,
                                       const gchar *format,
                                       ...);

G_END_DECLS

#endif /* __PEAS_PYTHON_INTERNAL_H__ */
